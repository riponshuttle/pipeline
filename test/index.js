var http = require('http');
var srvr = http.createServer(function (req, res) {
  res.write('Hello World Server!');
  res.end();
});
srvr.listen(8080);
var expect  = require('chai').expect;
var request = require('request');

it('Main page content', function(done) {
    request('http://localhost:8080' , function(error, response, body) {
        console.log('Hello World');
        done();
        srvr.close();
    });
});